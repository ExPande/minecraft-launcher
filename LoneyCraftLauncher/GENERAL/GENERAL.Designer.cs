﻿namespace LoneyCraftLauncher.GENERAL
{
    partial class GENERAL
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Button_Settings = new System.Windows.Forms.Button();
            this.Button_Update = new System.Windows.Forms.Button();
            this.Button_Game = new System.Windows.Forms.Button();
            this.TextBox_Password = new System.Windows.Forms.TextBox();
            this.TextBox_Login = new System.Windows.Forms.TextBox();
            this.WebBrowser_News = new System.Windows.Forms.WebBrowser();
            this.Timer_LoginPassword = new System.Windows.Forms.Timer(this.components);
            this.CheckBox_isPirate = new System.Windows.Forms.CheckBox();
            this.Label_isPirate = new System.Windows.Forms.Label();
            this.Label_Status = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Button_Settings
            // 
            this.Button_Settings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Button_Settings.Location = new System.Drawing.Point(3, 291);
            this.Button_Settings.Name = "Button_Settings";
            this.Button_Settings.Size = new System.Drawing.Size(165, 23);
            this.Button_Settings.TabIndex = 0;
            this.Button_Settings.Text = "Удалить MINECRAFT";
            this.Button_Settings.UseVisualStyleBackColor = true;
            this.Button_Settings.Click += new System.EventHandler(this.Button_Settings_Click);
            // 
            // Button_Update
            // 
            this.Button_Update.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Update.Enabled = false;
            this.Button_Update.Location = new System.Drawing.Point(505, 292);
            this.Button_Update.Name = "Button_Update";
            this.Button_Update.Size = new System.Drawing.Size(82, 23);
            this.Button_Update.TabIndex = 1;
            this.Button_Update.Text = "Установить";
            this.Button_Update.UseVisualStyleBackColor = true;
            this.Button_Update.Click += new System.EventHandler(this.Button_Update_Click);
            // 
            // Button_Game
            // 
            this.Button_Game.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Game.Location = new System.Drawing.Point(505, 263);
            this.Button_Game.Name = "Button_Game";
            this.Button_Game.Size = new System.Drawing.Size(82, 23);
            this.Button_Game.TabIndex = 2;
            this.Button_Game.Text = "Играть";
            this.Button_Game.UseVisualStyleBackColor = true;
            this.Button_Game.Click += new System.EventHandler(this.Button_Game_Click);
            // 
            // TextBox_Password
            // 
            this.TextBox_Password.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBox_Password.Enabled = false;
            this.TextBox_Password.Location = new System.Drawing.Point(366, 294);
            this.TextBox_Password.MaxLength = 32;
            this.TextBox_Password.Name = "TextBox_Password";
            this.TextBox_Password.Size = new System.Drawing.Size(133, 20);
            this.TextBox_Password.TabIndex = 3;
            this.TextBox_Password.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TextBox_Password.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TextBox_Password_MouseClick);
            this.TextBox_Password.TextChanged += new System.EventHandler(this.TextBox_Password_TextChanged);
            this.TextBox_Password.Leave += new System.EventHandler(this.TextBox_Password_Leave);
            // 
            // TextBox_Login
            // 
            this.TextBox_Login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBox_Login.Location = new System.Drawing.Point(366, 265);
            this.TextBox_Login.MaxLength = 32;
            this.TextBox_Login.Name = "TextBox_Login";
            this.TextBox_Login.Size = new System.Drawing.Size(133, 20);
            this.TextBox_Login.TabIndex = 4;
            this.TextBox_Login.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TextBox_Login.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TextBox_Login_MouseClick);
            this.TextBox_Login.TextChanged += new System.EventHandler(this.TextBox_Login_TextChanged);
            this.TextBox_Login.Leave += new System.EventHandler(this.TextBox_Login_Leave);
            // 
            // WebBrowser_News
            // 
            this.WebBrowser_News.AllowWebBrowserDrop = false;
            this.WebBrowser_News.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WebBrowser_News.IsWebBrowserContextMenuEnabled = false;
            this.WebBrowser_News.Location = new System.Drawing.Point(3, 3);
            this.WebBrowser_News.MinimumSize = new System.Drawing.Size(20, 20);
            this.WebBrowser_News.Name = "WebBrowser_News";
            this.WebBrowser_News.ScrollBarsEnabled = false;
            this.WebBrowser_News.Size = new System.Drawing.Size(584, 237);
            this.WebBrowser_News.TabIndex = 5;
            this.WebBrowser_News.TabStop = false;
            this.WebBrowser_News.Url = new System.Uri("", System.UriKind.Relative);
            this.WebBrowser_News.Visible = false;
            this.WebBrowser_News.WebBrowserShortcutsEnabled = false;
            this.WebBrowser_News.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.WebBrowser_News_DocumentCompleted);
            // 
            // Timer_LoginPassword
            // 
            this.Timer_LoginPassword.Tick += new System.EventHandler(this.Timer_LoginPassword_Tick);
            // 
            // CheckBox_isPirate
            // 
            this.CheckBox_isPirate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckBox_isPirate.AutoSize = true;
            this.CheckBox_isPirate.Checked = true;
            this.CheckBox_isPirate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_isPirate.Location = new System.Drawing.Point(572, 246);
            this.CheckBox_isPirate.Name = "CheckBox_isPirate";
            this.CheckBox_isPirate.Size = new System.Drawing.Size(15, 14);
            this.CheckBox_isPirate.TabIndex = 6;
            this.CheckBox_isPirate.UseVisualStyleBackColor = true;
            this.CheckBox_isPirate.CheckedChanged += new System.EventHandler(this.CheckBox_isPirate_CheckedChanged);
            // 
            // Label_isPirate
            // 
            this.Label_isPirate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_isPirate.AutoSize = true;
            this.Label_isPirate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Label_isPirate.Location = new System.Drawing.Point(502, 244);
            this.Label_isPirate.Name = "Label_isPirate";
            this.Label_isPirate.Size = new System.Drawing.Size(64, 16);
            this.Label_isPirate.TabIndex = 7;
            this.Label_isPirate.Text = "Пиратка";
            // 
            // Label_Status
            // 
            this.Label_Status.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label_Status.AutoSize = true;
            this.Label_Status.Font = new System.Drawing.Font("Candara", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Status.ForeColor = System.Drawing.Color.White;
            this.Label_Status.Location = new System.Drawing.Point(3, 245);
            this.Label_Status.Name = "Label_Status";
            this.Label_Status.Size = new System.Drawing.Size(89, 24);
            this.Label_Status.TabIndex = 8;
            this.Label_Status.Text = "Загрузка";
            this.Label_Status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // GENERAL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.Label_isPirate);
            this.Controls.Add(this.TextBox_Login);
            this.Controls.Add(this.TextBox_Password);
            this.Controls.Add(this.Button_Game);
            this.Controls.Add(this.Button_Update);
            this.Controls.Add(this.Button_Settings);
            this.Controls.Add(this.CheckBox_isPirate);
            this.Controls.Add(this.Label_Status);
            this.Controls.Add(this.WebBrowser_News);
            this.Name = "GENERAL";
            this.Size = new System.Drawing.Size(590, 318);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Button_Settings;
        private System.Windows.Forms.TextBox TextBox_Password;
        private System.Windows.Forms.TextBox TextBox_Login;
        private System.Windows.Forms.Timer Timer_LoginPassword;
        private System.Windows.Forms.CheckBox CheckBox_isPirate;
        private System.Windows.Forms.Label Label_isPirate;
        public System.Windows.Forms.WebBrowser WebBrowser_News;
        public System.Windows.Forms.Button Button_Update;
        public System.Windows.Forms.Label Label_Status;
        public System.Windows.Forms.Button Button_Game;
    }
}
