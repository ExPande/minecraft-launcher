﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Security.Permissions;

namespace LoneyCraftLauncher.GENERAL
{
    [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
    public partial class GENERAL : UserControl
    {
        private bool isLoad = true;

        public GENERAL()
        {
            InitializeComponent();
        }

        public void FormOpen()
        {
            if (isLoad)
            {
                if (Library.LoadKey("isPirate") == "true" || Library.LoadKey("isPirate") == "")
                {
                    CheckBox_isPirate.Checked = true;
                }
                else
                {
                    CheckBox_isPirate.Checked = false;
                }

                if (Library.LoadKey("Login") == "")
                {
                    TextBox_Login.Text = "Логин";
                }
                else
                {
                    TextBox_Login.Text = Library.LoadKey("Login");
                }

                if (Library.LoadKey("Password") == "")
                {
                    TextBox_Password.Text = "Пароль";
                }
                else
                {
                    TextBox_Password.Text = Library.Decrypt(Library.LoadKey("Password"));
                }

                Button_Game.Select();

                isLoad = false;
            }

            if (Directory.Exists(Program.APPDATA +  @"\.minecraft"))
            {
                Button_Update.Text = "Обновить";
                MAIN._UPDATE.Button_Update.Text = "Обновить";
                Button_Settings.Enabled = true;
            }
            else
            {
                Button_Update.Text = "Установить";
                MAIN._UPDATE.Button_Update.Text = "Установить";
                Button_Settings.Enabled = false;
            }
        }


        #region WebBrowser
        private void WebBrowser_News_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (WebBrowser_News.Url == new Uri(String.Format("file:///" + MAIN.LONEYCRAFTPATH + @"Cache\index.html")))
            {
                WebBrowser_News.Visible = true;
            }
        }
        #endregion

        #region TextBox_Login
        private void TextBox_Login_MouseClick(object sender, MouseEventArgs e)
        {
            if (TextBox_Login.Text == "Логин")
            {
                TextBox_Login.Text = "";
            }
        }

        private void TextBox_Login_TextChanged(object sender, System.EventArgs e)
        {
            if (TextBox_Login.Text == "Логин" || TextBox_Login.Text == "")
            {
                Library.SaveKey("Login", null);
            }
            else
            {
                Library.SaveKey("Login", TextBox_Login.Text);
            }
        }

        private void TextBox_Login_Leave(object sender, System.EventArgs e)
        {
            if (TextBox_Login.Text == "")
            {
                TextBox_Login.Text = "Логин";
            }
        }
        #endregion

        #region TextBox_Password
        private void TextBox_Password_MouseClick(object sender, MouseEventArgs e)
        {
            if (TextBox_Password.Text == "Пароль")
            {
                TextBox_Password.Text = "";
            }
        }

        private void TextBox_Password_TextChanged(object sender, System.EventArgs e)
        {
            if (TextBox_Password.Text == "Пароль")
            {
                TextBox_Password.UseSystemPasswordChar = false;
                Library.SaveKey("Password", null);
            }
            else
            {
                TextBox_Password.UseSystemPasswordChar = true;
                Library.SaveKey("Password", Library.Encrypt(TextBox_Password.Text));
            }
        }

        private void TextBox_Password_Leave(object sender, System.EventArgs e)
        {
            if (TextBox_Password.Text == "")
            {
                TextBox_Password.Text = "Пароль";
            }
        }
        #endregion

        #region Timer_LoginPassword
        private int made, last = 3;
        private void Timer_LoginPassword_Tick(object sender, System.EventArgs e)
        {
            if (made <= last)
            {
                if (TextBox_Login.Text == "Логин")
                {
                    if (TextBox_Login.Enabled == true)
                    {
                        TextBox_Login.Enabled = false;
                    }
                    else
                    {
                        TextBox_Login.Enabled = true;
                    }
                }

                if (TextBox_Password.Text == "Пароль" && CheckBox_isPirate.Checked == false)
                {
                    if (TextBox_Password.Enabled == true)
                    {
                        TextBox_Password.Enabled = false;
                    }
                    else
                    {
                        TextBox_Password.Enabled = true;
                    }
                }
                made++;
            }
            else
            {
                made = 0;
                Timer_LoginPassword.Stop();
            }
        }
        #endregion

        private void CheckBox_isPirate_CheckedChanged(object sender, System.EventArgs e)
        {
            if (CheckBox_isPirate.Checked == true)
            {
                TextBox_Password.Enabled = false;
                Library.SaveKey("isPirate", "true");
            }
            else
            {
                TextBox_Password.Enabled = true;
                Library.SaveKey("isPirate", "false");
            }
        }

        // Кнопка УСТАНОВИТЬ
        private void Button_Update_Click(object sender, System.EventArgs e)
        {
            Library.FormOpen("_UPDATE");
        }

        // Кнопка НАСТРОЙКИ
        private void Button_Settings_Click(object sender, System.EventArgs e)
        {
            if (MessageBox.Show("Вы уверены, что хотите удалить игру?", "Удаление Minecraft", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                MAIN._UPDATE.SetStatus("Удаление Мinecraft");
                Library.DirectoryRemove(Program.APPDATA + @"\.minecraft\");
                if (!Directory.Exists(Program.APPDATA + @"\.minecraft\"))
                {
                    MAIN._UPDATE.SetStatus();
                    MessageBox.Show("Игра успешно удалена!");
                    UPDATE.OpenXML.isLoad = true;
                    Button_Settings.Enabled = false;
                }
                UPDATE.OpenXML.Reduction();
                FormOpen();
            }
        }

        // Кнопка ИГРАТЬ
        private void Button_Game_Click(object sender, System.EventArgs e)
        {
            if (Library.JavaPath() != null)
            {
                if (File.Exists(Program.APPDATA + @"\.minecraft\bin\minecraft.jar"))
                {
                    if (CheckBox_isPirate.Checked)
                    {
                        if (TextBox_Login.Text == "Логин")
                        {
                            Timer_LoginPassword.Start();
                        }
                        else
                        {
                            Process.Start(Library.JavaPath(), Environment.ExpandEnvironmentVariables("-Xincgc -Xmx1G -cp \"%APPDATA%\\.minecraft\\bin\\minecraft.jar;%APPDATA%\\.minecraft\\bin\\lwjgl.jar;%APPDATA%\\.minecraft\\bin\\lwjgl_util.jar;%APPDATA%\\.minecraft\\bin\\jinput.jar\" -Djava.library.path=\"%APPDATA%\\.minecraft\\bin\\natives\" net.minecraft.client.Minecraft \"" + TextBox_Login.Text + "\""));
                            Application.Exit();
                        }
                    }
                    else
                    {
                        if (TextBox_Login.Text == "Логин" || TextBox_Password.Text == "Пароль")
                        {
                            Timer_LoginPassword.Start();
                        }
                        else
                        {
                            Process.Start(Library.JavaPath(), Environment.ExpandEnvironmentVariables("-Xincgc -Xmx1G -cp \"%APPDATA%\\.minecraft\\bin\\minecraft.jar;%APPDATA%\\.minecraft\\bin\\lwjgl.jar;%APPDATA%\\.minecraft\\bin\\lwjgl_util.jar;%APPDATA%\\.minecraft\\bin\\jinput.jar\" -Djava.library.path=\"%APPDATA%\\.minecraft\\bin\\natives\" net.minecraft.client.Minecraft \"" + TextBox_Login.Text + "\" \"" + TextBox_Password + "\""));
                            Application.Exit();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Не найден дистрибутив игры!\n Для установки игры следует перейти по кнопке Обновить(Установить).", "LoneyCraft", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Не найдено программное обеспечение Java!\n Для установки программного обеспечения Java следует перейти по кнопке Обновить(Установить).", "LoneyCraft", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
