﻿using System;
using System.ComponentModel;
using System.Deployment.Application;
using System.IO;
using System.Net;
using System.Windows.Forms;
using LoneyCraftLauncher.Properties;

namespace LoneyCraftLauncher
{
    public partial class MAIN : Form
    {
        public static GENERAL.GENERAL _GENERAL;
        public static UPDATE.UPDATE _UPDATE;
        public static bool CONNECTION = false;
        public static string JAVAPATH = null;
        
        public static string LONEYCRAFTPATH = Program.APPDATA + @"\LoneyCraft\";

        public const string FTPLOGIN = "loneycraft";
        public const string FTPPASSWORD = "loneycraft";
        public const string URLFTP = "ftp://loneycraft.ru/";
        public const string URLHTTP = "http://launcher.loneycraft.ru/";
        

        public MAIN()
        {
            InitializeComponent();

            _GENERAL = new GENERAL.GENERAL();
            _GENERAL.BackColor = System.Drawing.Color.Transparent;
            _GENERAL.Location = new System.Drawing.Point(0, 0);
            _GENERAL.Size = this.ClientSize;
            _GENERAL.TabIndex = 1;
            _GENERAL.Visible = false;
            Controls.Add(_GENERAL);

            _UPDATE = new UPDATE.UPDATE();
            _UPDATE.BackColor = System.Drawing.Color.Transparent;
            _UPDATE.Location = new System.Drawing.Point(0, 0);
            _UPDATE.Size = this.ClientSize;
            _UPDATE.TabIndex = 1;
            _UPDATE.Visible = false;
            Controls.Add(_UPDATE);
        }

        private void MAIN_Load(object sender, System.EventArgs e)
        {
            Version version = Library.GetPublishedVersion();
            Text += "   v" + version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Revision.ToString();

            // Загрузка тугодумных функций
            using (BackgroundWorker BWorker = new BackgroundWorker())
            {
                BWorker.WorkerReportsProgress = true;
                BWorker.DoWork += new DoWorkEventHandler(BWorker_DoWork);
                BWorker.ProgressChanged += new ProgressChangedEventHandler(BWorker_ProgressChanged);
                BWorker.RunWorkerAsync();
            }

            //Stopwatch sWatch = new Stopwatch();
            //sWatch.Start();
                
            // Открытие главной формы
            MAIN._GENERAL.FormOpen();
            MAIN._GENERAL.Visible = true;

            //sWatch.Stop();
            //sWatch.ElapsedMilliseconds.ToString();

            // Включение плавного появления
            Timer_Opacity.Enabled = true;
        }

        private void Timer_Opacity_Tick(object sender, System.EventArgs e)
        {
            if (Opacity <= 1)
            {
                Opacity += 0.1;
            }
            else
            {
                Timer_Opacity.Enabled = false;
            }
        }

        private void BWorker_DoWork(Object sender, DoWorkEventArgs e)
        {
            try
            {
                if (ApplicationDeployment.IsNetworkDeployed && Settings.Default.IsFirstRun)
                {
                    bool createshortcut = true;
                    string[] files = null;

                    // Поиск ярлыка
                    try
                    {
                        files = Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "*.appref-ms", SearchOption.AllDirectories);
                    }
                    catch (UnauthorizedAccessException)
                    {
                        files = Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "*.appref-ms", SearchOption.TopDirectoryOnly);
                    }

                    foreach (string file in files)
                    {
                        if (File.ReadAllText(file).IndexOf("LoneyCraftLauncher.application") != -1)
                        {
                            createshortcut = false;
                        }
                    }


                    // Создание ярлыка на рабочем столе
                    if (createshortcut)
                    {
                        if (MessageBox.Show("Создать ярлык на рабочем столе?", "LoneyCraft Лаунчер", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            string[] shortcuts = Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.Programs) + @"\LoneyCraft\", "*.appref-ms");
                            foreach (string file in shortcuts)
                            {
                                Library.FileCopy(file, Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\LoneyCraft.appref-ms");
                            }
                        }
                    }

                    Settings.Default.IsFirstRun = false;
                    Settings.Default.Save();

                    /* ТОЛЬКО ДЛЯ ВЕРСИИ */
                    Library.DirectoryRemove(Environment.GetEnvironmentVariable("ALLUSERSPROFILE") + @"\LoneyCraft\");
                }

                (sender as BackgroundWorker).ReportProgress(0, "Проверка соединения с сервером");
                // Проверка соединения с сервером
                CONNECTION = NETEx.Ping("178.173.20.162");

                if (CONNECTION)
                {
                    try
                    {
                        // Загрузка новостей
                        (sender as BackgroundWorker).ReportProgress(0, "Загрузка новостей");
                        string body = Library.getHTML(URLHTTP + "body.php");

                        // Проверка файлов кэш
                        (sender as BackgroundWorker).ReportProgress(0, "Проверка кэш файлов 1/11");
                        ExistsAndDownload("style.css", body);

                        (sender as BackgroundWorker).ReportProgress(0, "Проверка кэш файлов 2/11");
                        ExistsAndDownload("jscrollpane.css", body);

                        (sender as BackgroundWorker).ReportProgress(0, "Проверка кэш файлов 3/11");
                        ExistsAndDownload("jquery.js", body);

                        (sender as BackgroundWorker).ReportProgress(0, "Проверка кэш файлов 4/11");
                        ExistsAndDownload("jquery.mousewheel.min.js", body);

                        (sender as BackgroundWorker).ReportProgress(0, "Проверка кэш файлов 5/11");
                        ExistsAndDownload("jscrollpane.js", body);

                        (sender as BackgroundWorker).ReportProgress(0, "Проверка кэш файлов 6/11");
                        ExistsAndDownload(@"images\background.png", body);

                        (sender as BackgroundWorker).ReportProgress(0, "Проверка кэш файлов 7/11");
                        ExistsAndDownload(@"images\background_box.png", body);

                        (sender as BackgroundWorker).ReportProgress(0, "Проверка кэш файлов 8/11");
                        ExistsAndDownload(@"images\drag.png", body);

                        (sender as BackgroundWorker).ReportProgress(0, "Проверка кэш файлов 9/11");
                        ExistsAndDownload(@"images\but.png", body);

                        (sender as BackgroundWorker).ReportProgress(0, "Проверка кэш файлов 10/11");
                        ExistsAndDownload("head.template", body);

                        (sender as BackgroundWorker).ReportProgress(0, "Проверка кэш файлов 11/11");
                        ExistsAndDownload("foot.template", body);

                        (sender as BackgroundWorker).ReportProgress(0, "Обработка новостей");
                        File.WriteAllText(MAIN.LONEYCRAFTPATH + @"Cache\index.html", File.ReadAllText(MAIN.LONEYCRAFTPATH + @"Cache\head.template") + body + File.ReadAllText(MAIN.LONEYCRAFTPATH + @"Cache\foot.template"));
                    }
                    catch (WebException) 
                    {
                        CONNECTION = false;
                    }
                }

                (sender as BackgroundWorker).ReportProgress(1);

                // Поиск папки Java
                JAVAPATH = Library.JavaPath();
            }
            catch (UnauthorizedAccessException exception)
            {
                (sender as BackgroundWorker).ReportProgress(200, exception.Message);
            }
        }

        private void ExistsAndDownload(string file, string html)
        {
            if (!File.Exists(LONEYCRAFTPATH + @"Cache\" + file) || html.IndexOf("<!-- " + file + "->" + Library.GetMD5HashFromFile(LONEYCRAFTPATH + @"Cache\" + file) + " -->") == -1)
            {
                Library.FileDelete(LONEYCRAFTPATH + @"Cache\" + file);
                WebClient Client = new WebClient();
                Client.Dispose();
                Client.DownloadFile(URLHTTP + file, Library.Guide(LONEYCRAFTPATH + @"Cache\" + file));
            }
        }

        private void BWorker_ProgressChanged(Object sender, ProgressChangedEventArgs e)
        {
            switch (e.ProgressPercentage)
            {
                case 0:
                    _UPDATE.SetStatus((string)e.UserState);
                    break;
                case 1:
                    if (CONNECTION)
                    {
                        UPDATE.OpenXML.Open();
                        _GENERAL.WebBrowser_News.Navigate(new Uri(String.Format("file:///" + MAIN.LONEYCRAFTPATH + @"Cache\index.html")));
                    }
                    else
                    {
                        _UPDATE.SetStatus("Сервер недоступен!");
                    }
                    break;
                case 200:
                    MessageBox.Show((string)e.UserState, "Ошибка в приложении LoneyCraft", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Environment.Exit(0);
                    break;
            }
        }
    }
}
