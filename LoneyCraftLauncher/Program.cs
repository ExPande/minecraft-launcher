﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace LoneyCraftLauncher
{
    static class Program
    {
        public static string APPDATA = Environment.GetEnvironmentVariable("APPDATA");

        delegate bool EnumThreadWindowsCallback(IntPtr hWnd, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool EnumWindows([In] EnumThreadWindowsCallback callback, [In] IntPtr extraData);

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow([In] IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool ShowWindow([In] IntPtr hWnd, [In] int nCmdShow);

        [DllImport("user32.dll", SetLastError = true)]
        static extern int GetWindowThreadProcessId(IntPtr handle, out int processId);

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr GetWindow(IntPtr hWnd, int uCmd);

        static Mutex mutex;

        [STAThread]
        private static void Main()
        {
            // Глобальная обработка ошибок
            AppDomain.CurrentDomain.UnhandledException += delegate(object sender, UnhandledExceptionEventArgs args)
            {
                Library.NotifyErrorAndCloseApplication(args.ExceptionObject.ToString());
            };
            Application.ThreadException += delegate(Object sender, ThreadExceptionEventArgs args)
            {
                Library.NotifyErrorAndCloseApplication(args.Exception.ToString());
            };

            // Запрет на запуск нескольких копий приложения
            if (IsRunning()) 
            {
                int PID;
                string procName;

                using(var process = Process.GetCurrentProcess()) 
                {
                    PID = process.Id;
                    procName = process.ProcessName;
                }

                IntPtr hWin = IntPtr.Zero;

                foreach (var proc in Process.GetProcessesByName(procName))
                {
                    if (proc.Id != PID)
                    {
                        hWin = MainWindow(proc.Id);
                        ShowWindow(hWin, 9);
                        SetForegroundWindow(hWin);
                    }
                }
                Environment.Exit(0);
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MAIN());
            mutex.Close();
        }

        
        static bool IsRunning()
        {
            bool created;
            mutex = new Mutex(true, "LoneyCraftLauncher", out created);
            return !created;
        }

        static IntPtr MainWindow(int PID)
        {
            int getPID;
            IntPtr hMainWin = IntPtr.Zero;
            EnumWindows(new EnumThreadWindowsCallback((hWnd, Zero) =>
            {
                GetWindowThreadProcessId(hWnd, out getPID);

                if (getPID == PID && GetWindow(hWnd, 4) == IntPtr.Zero)
                {
                    hMainWin = hWnd;
                    return false;
                }

                return true;
            }), IntPtr.Zero);

            return hMainWin;
        }
    }
}
