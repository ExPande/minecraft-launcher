﻿using System;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;

namespace LoneyCraftLauncher
{
    public class DoFileEventArgs : EventArgs {}

    static public class NETEx
    {
        public delegate void DoFile(object sender, DoFileEventArgs e);

        public static event DoFile ProgressChanged;

        /// <summary>
        /// Проверяет соединение с сервером
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static bool ConnectionAvailable(string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (HttpStatusCode.OK == response.StatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (WebException)
            {
                return false;
            }
        }

        public static bool Ping(string url)
        {
            using (Ping ping = new Ping())
                if (ping.Send(url, 120).Status == IPStatus.Success)
                    return true;
            return false;
        }

        public static bool FTP(string from, string to, string login, string password)
        {
            try
            {
                FtpWebRequest Request = FtpWebRequest.Create(from) as FtpWebRequest;
                Request.Credentials = new NetworkCredential(login, password);
                Request.KeepAlive = true;
                Request.Method = WebRequestMethods.Ftp.GetFileSize;
                Request.UseBinary = true;
                Request.Proxy = null;
                Request.UsePassive = true;
                Request.Timeout = 15000;
                Int64 DataLength = Request.GetResponse().ContentLength;

                Request = FtpWebRequest.Create(from) as FtpWebRequest;
                Request.Credentials = new NetworkCredential(login, password);
                Request.KeepAlive = false;
                Request.Method = WebRequestMethods.Ftp.DownloadFile;
                Request.UseBinary = true;
                Request.Proxy = null;
                Request.UsePassive = true;
                Request.Timeout = 15000;
                FtpWebResponse Response = (FtpWebResponse)Request.GetResponse();
                Stream Reader = Response.GetResponseStream();
                using (FileStream Writer = new FileStream(to, FileMode.Create))
                {
                    Byte[] buffer = new Byte[2048];
                    Int64 isDataLength = 0;
                    while (true)
                    {
                        int bytesRead = Reader.Read(buffer, 0, buffer.Length);
                        if (bytesRead == 0)
                        {
                            break;
                        }
                        else
                        {
                            Writer.Write(buffer, 0, bytesRead);
                            if (isDataLength + bytesRead <= DataLength)
                            {
                                isDataLength += bytesRead;
                                ProgressChanged((int)(((double)isDataLength / (double)DataLength) * 100), new DoFileEventArgs());
                            }
                        }
                    }
                }
                return true;
            }
            catch (WebException)
            {
                return false;
            }
        }

        public static bool FTPandProgress(string from, string to, string login, string password)
        {
            try
            {
                FtpWebRequest Request = FtpWebRequest.Create(from) as FtpWebRequest;
                Request.Credentials = new NetworkCredential(login, password);
                Request.KeepAlive = true;
                Request.Method = WebRequestMethods.Ftp.GetFileSize;
                Request.UseBinary = true;
                Request.Proxy = null;
                Request.UsePassive = true;
                Request.Timeout = 15000;
                Int64 DataLength = Request.GetResponse().ContentLength;

                Request = FtpWebRequest.Create(from) as FtpWebRequest;
                Request.Credentials = new NetworkCredential(login, password);
                Request.KeepAlive = false;
                Request.Method = WebRequestMethods.Ftp.DownloadFile;
                Request.UseBinary = true;
                Request.Proxy = null;
                Request.UsePassive = true;
                Request.Timeout = 15000;
                FtpWebResponse Response = (FtpWebResponse)Request.GetResponse();
                Stream Reader = Response.GetResponseStream();
                using (FileStream Writer = new FileStream(to, FileMode.Create))
                {
                    Byte[] buffer = new Byte[2048];
                    Int64 isDataLength = 0;
                    while (true)
                    {
                        int bytesRead = Reader.Read(buffer, 0, buffer.Length);
                        if (bytesRead == 0)
                        {
                            break;
                        }
                        else
                        {
                            Writer.Write(buffer, 0, bytesRead);
                            if (isDataLength + bytesRead <= DataLength)
                            {
                                isDataLength += bytesRead;
                                ProgressChanged((int)(((double)isDataLength / (double)DataLength) * 100), new DoFileEventArgs());
                            }
                        }
                    }
                }
                return true;
            }
            catch (WebException)
            {
                return false;
            }
        }

        public static bool HTTPandProgress(string from, string to)
        {
            Uri url = new Uri(from);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            response.Close();
            Int64 iSize = response.ContentLength;
            Int64 iRunningByteTotal = 0;
            using (WebClient client = new WebClient())
                using (Stream streamRemote = client.OpenRead(new Uri(from)))
                    using (Stream streamLocal = new FileStream(to, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        int iByteSize = 0;
                        byte[] byteBuffer = new byte[iSize];
                        while ((iByteSize = streamRemote.Read(byteBuffer, 0, byteBuffer.Length)) > 0)
                        {
                            streamLocal.Write(byteBuffer, 0, iByteSize);
                            iRunningByteTotal += iByteSize;
                            ProgressChanged((int)(((double)iRunningByteTotal / (double)byteBuffer.Length) * 100), new DoFileEventArgs());
                        }
                    }
            return true;
        }
    }
}
