﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using LoneyCraftLauncher.Lib;

namespace LoneyCraftLauncher.UPDATE
{
    public class Training
    {
        private static BackgroundWorker BWorker;
        private static bool isLoad = true;

        public static void Run()
        {
            if (isLoad)
            {
                BWorker = new BackgroundWorker();
                BWorker.WorkerReportsProgress = true;
                BWorker.WorkerSupportsCancellation = true;
                BWorker.DoWork += new DoWorkEventHandler(BWorker_DoWork);
                BWorker.ProgressChanged += new ProgressChangedEventHandler(BWorker_ProgressChanged);
                BWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BWorker_RunWorkerCompleted);
                isLoad = false;
            }
            MAIN._UPDATE.ProgressBar_Update.Maximum = 100;
            BWorker.RunWorkerAsync();
        }

        private static void BWorker_DoWork(Object sender, DoWorkEventArgs e)
        {
            if (!File.Exists(Program.APPDATA + @"\.minecraft\bin\minecraft.jar"))
            {
                for (int i = 0; i < OpenXML.AddonsValue; i++)
                {
                    if(OpenXML.Addons[i].Name.Equals("Minecraft", StringComparison.InvariantCultureIgnoreCase) && !MAIN._UPDATE.CheckedListBox_Addons.GetItemChecked(i))
                    {
                        e.Cancel = true;
                        return;
                    }
                }
            }

            BWorker.ReportProgress(0, "Подготовка временной папки");
            Library.DirectoryClean(MAIN.LONEYCRAFTPATH + @"Downloads\");
            Library.DirectoryClean(MAIN.LONEYCRAFTPATH + @"Temp\");


            if (Library.JavaPath() == null)
            {
                BWorker.ReportProgress(0, "Загрузка Java " + OSVersionInfo.OSBits.ToString());
                if (!File.Exists(MAIN.LONEYCRAFTPATH + @"Downloads\java.exe"))
                {
                    if (!NETEx.FTP(MAIN.URLFTP + "jre_" + OSVersionInfo.OSBits.ToString() + ".exe", Library.Guide(MAIN.LONEYCRAFTPATH + @"Downloads\java.exe"), MAIN.FTPLOGIN, MAIN.FTPPASSWORD))
                    {
                        NETEx.HTTPandProgress(MAIN.URLHTTP + "files/jre_" + OSVersionInfo.OSBits.ToString() + ".exe", Library.Guide(MAIN.LONEYCRAFTPATH + @"Downloads\java.exe"));
                    }
                }
                Process.Start(MAIN.LONEYCRAFTPATH + @"Downloads\java.exe");
            }

            BWorker.ReportProgress(0, "Думаю...");
        }

        private static void Report(int i)
        {
            BWorker.ReportProgress(i, "Загрузка Java " + OSVersionInfo.OSBits.ToString());
        }

        private static void BWorker_ProgressChanged(Object sender, ProgressChangedEventArgs e)
        {
            if ((string)e.UserState != MAIN._GENERAL.Label_Status.Text)
            {
                MAIN._UPDATE.SetStatus((string)e.UserState);
            }
            MAIN._UPDATE.ProgressBar_Update.Value += e.ProgressPercentage - MAIN._UPDATE.ProgressBar_Update.Value;
        }

        private static void BWorker_RunWorkerCompleted(Object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                MAIN._UPDATE.Button_Update.Enabled = true;
                MAIN._UPDATE.Button_Back.Enabled = true;
                MAIN._UPDATE.Button_Settings.Enabled = true;
                MAIN._UPDATE.CheckedListBox_Addons.Enabled = true;
                MessageBox.Show("Базовый компонент \"Minecraft\" не выбран!");
            }
            else
            {
                MAIN._UPDATE.CheckedListBox_Addons.SelectedIndex = -1;
                MAIN._UPDATE.DownloadAddons();
            }
        }
    }
}
