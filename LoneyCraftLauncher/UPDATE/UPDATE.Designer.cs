﻿namespace LoneyCraftLauncher.UPDATE
{
    partial class UPDATE
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.CheckedListBox_Addons = new System.Windows.Forms.CheckedListBox();
            this.ProgressBar_Update = new System.Windows.Forms.ProgressBar();
            this.Button_Update = new System.Windows.Forms.Button();
            this.Button_Back = new System.Windows.Forms.Button();
            this.Button_Settings = new System.Windows.Forms.Button();
            this.Label_Status = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CheckedListBox_Addons
            // 
            this.CheckedListBox_Addons.CheckOnClick = true;
            this.CheckedListBox_Addons.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.CheckedListBox_Addons.FormattingEnabled = true;
            this.CheckedListBox_Addons.IntegralHeight = false;
            this.CheckedListBox_Addons.Location = new System.Drawing.Point(3, 3);
            this.CheckedListBox_Addons.Name = "CheckedListBox_Addons";
            this.CheckedListBox_Addons.Size = new System.Drawing.Size(584, 223);
            this.CheckedListBox_Addons.TabIndex = 0;
            this.CheckedListBox_Addons.Visible = false;
            // 
            // ProgressBar_Update
            // 
            this.ProgressBar_Update.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ProgressBar_Update.Location = new System.Drawing.Point(3, 290);
            this.ProgressBar_Update.Name = "ProgressBar_Update";
            this.ProgressBar_Update.Size = new System.Drawing.Size(584, 25);
            this.ProgressBar_Update.TabIndex = 1;
            // 
            // Button_Update
            // 
            this.Button_Update.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Update.Location = new System.Drawing.Point(481, 232);
            this.Button_Update.Name = "Button_Update";
            this.Button_Update.Size = new System.Drawing.Size(106, 52);
            this.Button_Update.TabIndex = 2;
            this.Button_Update.Text = "Подождите...";
            this.Button_Update.UseVisualStyleBackColor = true;
            this.Button_Update.Click += new System.EventHandler(this.Button_Update_Click);
            // 
            // Button_Back
            // 
            this.Button_Back.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Back.Location = new System.Drawing.Point(389, 261);
            this.Button_Back.Name = "Button_Back";
            this.Button_Back.Size = new System.Drawing.Size(86, 23);
            this.Button_Back.TabIndex = 3;
            this.Button_Back.Text = "Назад";
            this.Button_Back.UseVisualStyleBackColor = true;
            this.Button_Back.Click += new System.EventHandler(this.Button_Back_Click);
            // 
            // Button_Settings
            // 
            this.Button_Settings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Settings.Location = new System.Drawing.Point(389, 232);
            this.Button_Settings.Name = "Button_Settings";
            this.Button_Settings.Size = new System.Drawing.Size(86, 23);
            this.Button_Settings.TabIndex = 4;
            this.Button_Settings.Text = "Настройки";
            this.Button_Settings.UseVisualStyleBackColor = true;
            this.Button_Settings.Visible = false;
            // 
            // Label_Status
            // 
            this.Label_Status.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label_Status.AutoSize = true;
            this.Label_Status.Font = new System.Drawing.Font("Candara", 15F, System.Drawing.FontStyle.Bold);
            this.Label_Status.ForeColor = System.Drawing.Color.White;
            this.Label_Status.Location = new System.Drawing.Point(3, 245);
            this.Label_Status.Name = "Label_Status";
            this.Label_Status.Size = new System.Drawing.Size(89, 24);
            this.Label_Status.TabIndex = 5;
            this.Label_Status.Text = "Загрузка";
            // 
            // UPDATE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Button_Settings);
            this.Controls.Add(this.Button_Back);
            this.Controls.Add(this.Button_Update);
            this.Controls.Add(this.ProgressBar_Update);
            this.Controls.Add(this.Label_Status);
            this.Controls.Add(this.CheckedListBox_Addons);
            this.Name = "UPDATE";
            this.Size = new System.Drawing.Size(590, 318);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.CheckedListBox CheckedListBox_Addons;
        private System.Windows.Forms.Label Label_Status;
        public System.Windows.Forms.ProgressBar ProgressBar_Update;
        public System.Windows.Forms.Button Button_Update;
        public System.Windows.Forms.Button Button_Back;
        public System.Windows.Forms.Button Button_Settings;
    }
}
