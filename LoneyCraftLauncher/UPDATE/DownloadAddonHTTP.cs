﻿using System;
using System.ComponentModel;
using System.Net;
using System.Security.Permissions;

namespace LoneyCraftLauncher.UPDATE
{
    [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
    public class DownloadAddonHTTP
    {
        private static WebClient BWorker;
        private static bool isLoad = true;

        public static void Run()
        {
            if (isLoad)
            {
                BWorker = new WebClient();
                BWorker.DownloadProgressChanged += new DownloadProgressChangedEventHandler(BWorker_ProgressChanged);
                BWorker.DownloadFileCompleted += new AsyncCompletedEventHandler(BWorker_RunWorkerCompleted);
                isLoad = false;
            }
            MAIN._UPDATE.ProgressBar_Update.Value = 0;
            MAIN._UPDATE.ProgressBar_Update.Maximum = 100;
            MAIN._UPDATE.SetStatus("(HTTP) Загрузка: " + OpenXML.Addons[MAIN._UPDATE.CheckedListBox_Addons.SelectedIndex].Title);

            BWorker.DownloadFileAsync(new Uri(MAIN.URLHTTP + "files/" + OpenXML.Addons[MAIN._UPDATE.CheckedListBox_Addons.SelectedIndex].File), Library.Guide(MAIN.LONEYCRAFTPATH + @"Downloads\" + OpenXML.Addons[MAIN._UPDATE.CheckedListBox_Addons.SelectedIndex].File));
        }

        private static void BWorker_ProgressChanged(Object sender, DownloadProgressChangedEventArgs e)
        {
            double bytesIn = double.Parse(e.BytesReceived.ToString());
            double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            double percentage = bytesIn / totalBytes * 100;
            MAIN._UPDATE.ProgressBar_Update.Value = int.Parse(Math.Truncate(percentage).ToString());
        }

        private static void BWorker_RunWorkerCompleted(Object sender, AsyncCompletedEventArgs e)
        {
            MAIN._UPDATE.DownloadAddons();
        }
    }
}
