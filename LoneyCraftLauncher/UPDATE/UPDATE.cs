﻿using System.IO;
using System.Security.Permissions;
using System.Windows.Forms;

namespace LoneyCraftLauncher.UPDATE
{
    [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
    public partial class UPDATE : UserControl
    {
        public bool isLoad = true, isFTP = true, isUpdate = false;

        public UPDATE()
        {
            InitializeComponent();
        }

        public void FormOpen()
        {
            if (this.isLoad)
            {
                OpenXML.Open();
                this.isLoad = false;
            }
            OpenXML.Reduction();
            if (Directory.Exists(Program.APPDATA + @"\.minecraft\"))
            {
                this.Button_Update.Text = "Обновить";
            }
            else
            {
                this.Button_Update.Text = "Установить";
            }
            Button_Update.Enabled = true;
            Button_Back.Enabled = true;
            Button_Settings.Enabled = true;
            CheckedListBox_Addons.Enabled = true;
            ProgressBar_Update.Value = 0;
            ProgressBar_Update.Style = ProgressBarStyle.Blocks;
        }

        public void DownloadAddons()
        {
            SetStatus();
            if (CheckedListBox_Addons.SelectedIndex + 1 < OpenXML.AddonsValue)
            {
                CheckedListBox_Addons.SelectedIndex++;
                if (CheckedListBox_Addons.GetItemChecked(CheckedListBox_Addons.SelectedIndex))
                {
                    if (isFTP)
                    {
                        DownloadAddonFTP.Run();
                        return;
                    }
                    else
                    {
                        DownloadAddonHTTP.Run();
                        return;
                    }
                }
                else
                {
                    DownloadAddons();
                    return;
                }
            }
            else
            {
                ProgressBar_Update.Style = ProgressBarStyle.Marquee;
                CheckedListBox_Addons.SelectedIndex = -1;
                InstallAddons();
                return;
            }
        }

        public void InstallAddons()
        {
            SetStatus();
            if (CheckedListBox_Addons.SelectedIndex + 1 < OpenXML.AddonsValue)
            {
                CheckedListBox_Addons.SelectedIndex++;
                if (CheckedListBox_Addons.GetItemChecked(CheckedListBox_Addons.SelectedIndex))
                {
                    InstallAddon.Install(CheckedListBox_Addons.SelectedIndex);
                    return;
                }
                else
                {
                    //Library.SaveKey("~" + OpenXML.Addons[CheckedListBox_Addons.SelectedIndex].Name, null);
                    InstallAddons();
                    return;
                }
            }
            else
            {
                InstallAddon.Install(-1);
                return;
            }
        }

        public void SetStatus(string status = null)
        {
            if (status == null)
            {
                Label_Status.Text = "";
            }
            else
            {
                if (status.IndexOf("!") == -1)
                {
                    if (status.Length >= 33)
                    {
                        Label_Status.Text = status.Substring(0, 33) + "...";
                    }
                    else
                    {
                        Label_Status.Text = status;
                    }
                }
                else
                {
                    Label_Status.Text = status;
                }
            }
            MAIN._GENERAL.Label_Status.Text = Label_Status.Text;
        }

        private void Button_Back_Click(object sender, System.EventArgs e)
        {
            Library.FormOpen("_GENERAL");
        }

        private void Button_Update_Click(object sender, System.EventArgs e)
        {
            Button_Update.Enabled = false;
            Button_Back.Enabled = false;
            Button_Settings.Enabled = false;
            CheckedListBox_Addons.Enabled = false;
            Training.Run();
        }
    }
}
