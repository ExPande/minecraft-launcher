﻿namespace LoneyCraftLauncher.UPDATE
{
    public class Addon
    {
        private string name = null,
                       title = null,
                       version = null,
                       file = null,
                       action = null,
                       path = null,
                       check = null,

                       tick = null;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public string Version
        {
            get { return version; }
            set { version = value; }
        }

        public string File
        {
            get { return file; }
            set { file = value; }
        }

        public string Action
        {
            get { return action; }
            set { action = value; }
        }

        public string Path
        {
            get { return path; }
            set { path = value; }
        }

        public string Check
        {
            get { return check; }
            set { check = value; }
        }

        public string Tick
        {
            get { return tick; }
            set { tick = value; }
        }
    }
}
