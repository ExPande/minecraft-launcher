﻿using System;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using ICSharpCode.SharpZipLib.Zip;

namespace LoneyCraftLauncher.UPDATE
{
    public class InstallAddon
    {
        private static BackgroundWorker BWorker;
        private static bool isLoad = true;
        private static FastZip ARCHIVER = new FastZip();

        public static void Install(int AddonNumber)
        {
            if (isLoad)
            {
                BWorker = new BackgroundWorker();
                BWorker.WorkerReportsProgress = true;
                BWorker.DoWork += new DoWorkEventHandler(BWorker_DoWork);
                BWorker.ProgressChanged += new ProgressChangedEventHandler(BWorker_ProgressChanged);
                BWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BWorker_RunWorkerCompleted);
                isLoad = false;
            }
            BWorker.RunWorkerAsync(AddonNumber);
        }

        private static void BWorker_DoWork(Object sender, DoWorkEventArgs e)
        {
            if ((int)e.Argument >= 0)
            {
                BWorker.ReportProgress(0, "Установка " + OpenXML.Addons[(int)e.Argument].Title);
                switch (OpenXML.Addons[(int)e.Argument].Action)
                {
                    case "copy":
                        try
                        {
                            Library.FileCopy(MAIN.LONEYCRAFTPATH + @"\Downloads\" + OpenXML.Addons[(int)e.Argument].File, Library.Guide(Environment.ExpandEnvironmentVariables(OpenXML.Addons[(int)e.Argument].Path + OpenXML.Addons[(int)e.Argument].Name + OpenXML.Addons[(int)e.Argument].File.Substring(OpenXML.Addons[(int)e.Argument].File.LastIndexOf("."), OpenXML.Addons[(int)e.Argument].File.Length - OpenXML.Addons[(int)e.Argument].File.LastIndexOf(".")))));
                        }
                        catch (IOException exception)
                        {
                            if (MessageBox.Show(exception.Message + "\n Эта ошибка часто возникает, если процесс игры не был закрыт. Убедитесь, что игра закрыта!", "Ошибка в приложении LoneyCraft", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Retry)
                            {
                                e.Result = "retry";
                            }
                            else
                            {
                                e.Result = "finish";
                            }
                            return;
                        }
                        break;
                    case "extract":
                        try
                        {
                            if (OpenXML.Addons[(int)e.Argument].File == "minecraft.zip")
                            {
                                Library.DirectoryClean(Program.APPDATA + @"\.minecraft\mods\");
                            }
                            ARCHIVER.ExtractZip(MAIN.LONEYCRAFTPATH + @"Downloads\" + OpenXML.Addons[(int)e.Argument].File, Environment.ExpandEnvironmentVariables(OpenXML.Addons[(int)e.Argument].Path), null);
                        }
                        catch (UnauthorizedAccessException exception)
                        {
                            if (MessageBox.Show(exception.Message, "Ошибка в приложении LoneyCraft", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Retry)
                            {
                                e.Result = "retry";
                            }
                            else
                            {
                                e.Result = "finish";
                            }
                            return;
                        }
                        catch (IOException exception)
                        {
                            if (MessageBox.Show(exception.Message + "\n Эта ошибка часто возникает, если процесс игры не был закрыт. Убедитесь, что игра закрыта!", "Ошибка в приложении LoneyCraft", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Retry)
                            {
                                e.Result = "retry";
                            }
                            else
                            {
                                e.Result = "finish";
                            }
                            return;
                        }
                        break;
                    case "minecraft.jar":
                        try
                        {
                            if (!Directory.Exists(MAIN.LONEYCRAFTPATH + @"Temp\minecraft.jar\"))
                            {
                                BWorker.ReportProgress(0, "Распаковка minecraft.jar");
                                ARCHIVER.ExtractZip(Program.APPDATA + @"\.minecraft\bin\minecraft.jar", MAIN.LONEYCRAFTPATH + @"Temp\minecraft.jar\", null);
                                Library.DirectoryRemove(MAIN.LONEYCRAFTPATH + @"Temp\minecraft.jar\META-INF\");
                            }
                            BWorker.ReportProgress(0, "Установка " + OpenXML.Addons[(int)e.Argument].Title);
                            ARCHIVER.ExtractZip(MAIN.LONEYCRAFTPATH + @"Downloads\" + OpenXML.Addons[(int)e.Argument].File, MAIN.LONEYCRAFTPATH + @"Temp\minecraft.jar\", null);
                        }
                        catch (UnauthorizedAccessException exception)
                        {
                            if (MessageBox.Show(exception.Message, "Ошибка в приложении LoneyCraft", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Retry)
                            {
                                e.Result = "retry";
                            }
                            else
                            {
                                e.Result = "finish";
                            }
                            return;
                        }
                        catch (IOException exception)
                        {
                            if (MessageBox.Show(exception.Message + "\n Эта ошибка часто возникает, если процесс игры не был закрыт. Убедитесь, что игра закрыта!", "Ошибка в приложении LoneyCraft", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Retry)
                            {
                                e.Result = "retry";
                            }
                            else
                            {
                                e.Result = "finish";
                            }
                            return;
                        }
                        break;
                }
                Library.SaveKey("~" + OpenXML.Addons[(int)e.Argument].Name, OpenXML.Addons[(int)e.Argument].Version);
                e.Result = "next";
            }
            else
            {
                BWorker.ReportProgress(0, "Упаковка minecraft.jar");
                if (Directory.Exists(MAIN.LONEYCRAFTPATH + @"Temp\minecraft.jar\"))
                {
                    try
                    {
                        ARCHIVER.CreateZip(Program.APPDATA + @"\.minecraft\bin\minecraft.jar", MAIN.LONEYCRAFTPATH + @"Temp\minecraft.jar\", true, null);
                    }
                    catch (IOException exception)
                    {
                        if (MessageBox.Show(exception.Message + "\n Эта ошибка часто возникает, если процесс игры не был закрыт. Убедитесь, что игра закрыта!", "Ошибка в приложении LoneyCraft", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Retry)
                        {
                            e.Result = "retry_2";
                        }
                        else
                        {
                            e.Result = "finish";
                        }
                        return;
                    }
                }

                BWorker.ReportProgress(0, "Удаление временных файлов");
                Library.DirectoryRemove(MAIN.LONEYCRAFTPATH + @"Temp\");
                Library.DirectoryRemove(MAIN.LONEYCRAFTPATH + @"Downloads\");

                if (MAIN.JAVAPATH == null)
                {
                    BWorker.ReportProgress(0, "Поиск Java");
                    MAIN.JAVAPATH = Library.JavaPath();
                }

                Thread.Sleep(200);
                for (string dot = ""; dot.Length <= 3; dot += ".")
                {
                    BWorker.ReportProgress(0, "Готово" + dot);
                    Thread.Sleep(200);
                }
                BWorker.ReportProgress(0, "");
                e.Result = "finish";
            }
        }

        private static void BWorker_ProgressChanged(Object sender, ProgressChangedEventArgs e)
        {
            MAIN._UPDATE.SetStatus((string)e.UserState);
        }

        private static void BWorker_RunWorkerCompleted(Object sender, RunWorkerCompletedEventArgs e)
        {
            switch ((string)e.Result)
            {
                case "retry_2":
                    InstallAddon.Install(-1);
                    break;
                case "retry":
                    MAIN._UPDATE.CheckedListBox_Addons.SelectedIndex--;
                    MAIN._UPDATE.InstallAddons();
                    break;
                case "next":
                    MAIN._UPDATE.InstallAddons();
                    break;
                case "finish":
                    OpenXML.Open();
                    Library.FormOpen("_GENERAL");
                    break;
            }
        }
    }
}
