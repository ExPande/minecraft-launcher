﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;

namespace LoneyCraftLauncher.UPDATE
{
    public class DownloadAddonFTP
    {
        private static BackgroundWorker BWorker;
        private static Int32 DataLength = 0, isDataLength = 0;
        private static bool isLoad = true;

        public static void Run()
        {
            if (isLoad)
            {
                BWorker = new BackgroundWorker();
                BWorker.WorkerReportsProgress = true;
                BWorker.DoWork += new DoWorkEventHandler(BWorker_DoWork);
                BWorker.ProgressChanged += new ProgressChangedEventHandler(BWorker_ProgressChanged);
                BWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BWorker_RunWorkerCompleted);
                isLoad = false;
            }
            BWorker.RunWorkerAsync(MAIN._UPDATE.CheckedListBox_Addons.SelectedIndex);
        }

        private static void BWorker_DoWork(Object sender, DoWorkEventArgs e)
        {
            try
            {
                Byte[] DownloadData = new Byte[0];
                BWorker.ReportProgress(0, "(FTP) Получение сведений о:");
                FtpWebRequest Request = FtpWebRequest.Create(MAIN.URLFTP + OpenXML.Addons[(int)e.Argument].File) as FtpWebRequest;
                Request.Credentials = new NetworkCredential(MAIN.FTPLOGIN, MAIN.FTPPASSWORD);
                Request.KeepAlive = true;
                Request.Method = WebRequestMethods.Ftp.GetFileSize;
                Request.UseBinary = true;
                Request.Proxy = null;
                Request.UsePassive = true;
                Request.Timeout = 15000;
                DataLength = (Int32)Request.GetResponse().ContentLength;

                BWorker.ReportProgress(0, "(FTP) Загрузка:");
                Request = FtpWebRequest.Create(MAIN.URLFTP + OpenXML.Addons[(int)e.Argument].File) as FtpWebRequest;
                Request.Credentials = new NetworkCredential(MAIN.FTPLOGIN, MAIN.FTPPASSWORD);
                Request.KeepAlive = false;
                Request.Method = WebRequestMethods.Ftp.DownloadFile;
                Request.UseBinary = true;
                Request.Proxy = null;
                Request.UsePassive = true;
                Request.Timeout = 15000;
                BWorker.ReportProgress(1);
                FtpWebResponse Response = (FtpWebResponse)Request.GetResponse();
                Stream Reader = Response.GetResponseStream();
                using (FileStream Writer = new FileStream(Library.Guide(MAIN.LONEYCRAFTPATH + @"Downloads\" + OpenXML.Addons[(int)e.Argument].File), FileMode.Create))
                {
                    Byte[] buffer = new Byte[2048];
                    while (true)
                    {
                        int bytesRead = Reader.Read(buffer, 0, buffer.Length);
                        if (bytesRead == 0)
                        {
                            BWorker.ReportProgress(2);
                            break;
                        }
                        else
                        {
                            Writer.Write(buffer, 0, bytesRead);
                            if (isDataLength + bytesRead <= DataLength)
                            {
                                isDataLength += bytesRead;
                                BWorker.ReportProgress(3);
                            }
                        }
                    }
                }
                e.Result = "next";
            }
            catch (WebException)
            {
                MAIN._UPDATE.isFTP = false;
                e.Result = "retry";
            }
            catch (IOException)
            {
                MAIN._UPDATE.isFTP = false;
                e.Result = "retry";
            }
        }

        private static void BWorker_ProgressChanged(Object sender, ProgressChangedEventArgs e)
        {
            switch (e.ProgressPercentage)
            {
                case 0:
                    MAIN._UPDATE.SetStatus(e.UserState + " " + OpenXML.Addons[MAIN._UPDATE.CheckedListBox_Addons.SelectedIndex].Title);
                    break;
                case 1:
                    MAIN._UPDATE.ProgressBar_Update.Value = 0;
                    MAIN._UPDATE.ProgressBar_Update.Maximum = DataLength;
                    break;
                case 2:
                    MAIN._UPDATE.ProgressBar_Update.Value = MAIN._UPDATE.ProgressBar_Update.Maximum;
                    break;
                case 3:
                    MAIN._UPDATE.ProgressBar_Update.Value += isDataLength - MAIN._UPDATE.ProgressBar_Update.Value;
                    break;
            }
        }

        private static void BWorker_RunWorkerCompleted(Object sender, RunWorkerCompletedEventArgs e)
        {
            switch ((string)e.Result)
            {
                case "retry":
                    MAIN._UPDATE.CheckedListBox_Addons.SelectedIndex--;
                    MAIN._UPDATE.DownloadAddons();
                    break;
                case "next":
                    if (!File.Exists(MAIN.LONEYCRAFTPATH + @"Downloads\" + OpenXML.Addons[MAIN._UPDATE.CheckedListBox_Addons.SelectedIndex].File))
                    {
                        MAIN._UPDATE.CheckedListBox_Addons.SelectedIndex--;
                        MAIN._UPDATE.isFTP = false;
                    }
                    MAIN._UPDATE.DownloadAddons();
                    break;
            }
        }
    }
}
