﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Security.Permissions;
using System.Windows.Forms;
using System.Xml;

namespace LoneyCraftLauncher.UPDATE
{
    [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
    public class OpenXML
    {
        private static BackgroundWorker BWorker;
        public static Addon[] Addons = new Addon[64];
        public static int AddonsValue = 0;
        public static bool isLoad = true;

        public static void Open()
        {
            if (isLoad)
            {
                BWorker = new BackgroundWorker();
                BWorker.WorkerReportsProgress = true;
                BWorker.DoWork += new DoWorkEventHandler(BWorker_DoWork);
                BWorker.ProgressChanged += new ProgressChangedEventHandler(BWorker_ProgressChanged);
                BWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BWorker_RunWorkerCompleted);
                Addons = new Addon[64];
                AddonsValue = 0;
                BWorker.RunWorkerAsync();
                isLoad = false;
            }
        }

        private static void BWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            /*
            try
            {
                BWorker.ReportProgress(0, "(FTP) Загрузка файла версий");
                FtpWebRequest Request = (FtpWebRequest)WebRequest.Create(MAIN.URLFTP + "addons.xml");
                Request.Credentials = new NetworkCredential(MAIN.FTPLOGIN, MAIN.FTPPASSWORD);
                Request.KeepAlive = false;
                Request.Method = WebRequestMethods.Ftp.DownloadFile;
                Request.UseBinary = false;
                Request.Proxy = null;
                Request.UsePassive = true;
                Request.Timeout = 3000;

                response = (FtpWebResponse)Request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                XML = XmlReader.Create(responseStream);
            }
            */

            BWorker.ReportProgress(0, "(HTTP) Загрузка файла версий");

            try
            {
                WebRequest request = WebRequest.Create(MAIN.URLHTTP + "files/addons.xml");
                request.Timeout = 5000;
                WebResponse response = request.GetResponse();
                XmlReader XML = XmlReader.Create(response.GetResponseStream());

                // Разбор данных
                for (string Element = null; XML.Read(); )
                {
                    switch (XML.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (XML.Name == "addon")
                            {
                                Addons[AddonsValue] = new Addon();
                                AddonsValue++;
                            }
                            Element = XML.Name;
                            break;
                        case XmlNodeType.Text:
                            switch (Element)
                            {
                                case "name":
                                    Addons[AddonsValue - 1].Name = XML.Value;
                                    break;
                                case "title":
                                    Addons[AddonsValue - 1].Title = XML.Value;
                                    break;
                                case "version":
                                    Addons[AddonsValue - 1].Version = XML.Value;
                                    break;
                                case "file":
                                    Addons[AddonsValue - 1].File = XML.Value;
                                    break;
                                case "action":
                                    Addons[AddonsValue - 1].Action = XML.Value;
                                    break;
                                case "path":
                                    Addons[AddonsValue - 1].Path = XML.Value;
                                    break;
                                case "check":
                                    Addons[AddonsValue - 1].Check = XML.Value;
                                    break;
                                case "tick":
                                    Addons[AddonsValue - 1].Tick = XML.Value;
                                    break;
                            }
                            break;
                    }
                }

                XML.Close();
                response.Close();

                e.Result = "finish";
            }
            catch (WebException)
            {
                e.Result = "break";
            }
        }

        private static void BWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            MAIN._UPDATE.SetStatus((string)e.UserState);
        }

        private static void BWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            switch ((string)e.Result)
            {
                case "break":
                    MAIN.CONNECTION = false;
                    MAIN._UPDATE.SetStatus("Сервер недоступен!");
                    break;
                case "finish":
                    MAIN._GENERAL.Button_Update.Enabled = true;
                    Reduction();
                    break;
            }
        }

        public static void Reduction()
        {
            MAIN._UPDATE.SetStatus("Обработка XML");
            MAIN._UPDATE.CheckedListBox_Addons.Items.Clear();


            // Загрузка аддонов в CheckListBox
            for (int i = 0; i < AddonsValue; i++)
            {
                MAIN._UPDATE.CheckedListBox_Addons.Items.Add(Addons[i].Title, false);

                // Проверка <check>
                if (Addons[i].Check != null)
                {
                    if (Addons[i].Check.Substring(Addons[i].Check.Length - 1, 1) == @"\")
                    {
                        // Проверка существования папки
                        if (!Directory.Exists(Environment.ExpandEnvironmentVariables(Addons[i].Check)))
                        {
                            MAIN._UPDATE.CheckedListBox_Addons.SetItemChecked(i, true);
                        }
                    }
                    else
                    {
                        if (!File.Exists(Environment.ExpandEnvironmentVariables(Addons[i].Check)))
                        {
                            MAIN._UPDATE.CheckedListBox_Addons.SetItemChecked(i, true);
                        }
                    }
                }

                // Проверка <version>
                if (Addons[i].Version != null)
                {
                    if (Library.LoadKey("~" + Addons[i].Name) != Addons[i].Version)
                    {
                        MAIN._UPDATE.CheckedListBox_Addons.SetItemChecked(i, true);
                    }
                }

                // Проверка <tick>
                switch (Addons[i].Tick)
                {
                    case "true":
                        MAIN._UPDATE.CheckedListBox_Addons.SetItemChecked(i, true);
                        break;
                    case "false":
                        MAIN._UPDATE.CheckedListBox_Addons.SetItemChecked(i, false);
                        break;
                }
            }

            MAIN._UPDATE.SetStatus();
            MAIN._UPDATE.ProgressBar_Update.Value = 0;
            MAIN._UPDATE.ProgressBar_Update.Style = ProgressBarStyle.Blocks;

            MAIN._UPDATE.Button_Update.Enabled = true;
            MAIN._UPDATE.CheckedListBox_Addons.Visible = true;

            for (int i = 0; i < MAIN._UPDATE.CheckedListBox_Addons.Items.Count; i++)
            {
                if (MAIN._UPDATE.CheckedListBox_Addons.GetItemChecked(i))
                {
                    if (Directory.Exists(Program.APPDATA + @"\.minecraft"))
                    {
                        MAIN._UPDATE.SetStatus("Доступно обновление!");
                    }
                    else
                    {
                        MAIN._UPDATE.SetStatus("Нужно установить игру =)");
                    }
                    break;
                }
                MAIN._UPDATE.SetStatus();
            }
        }
    }
}
