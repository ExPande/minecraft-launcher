﻿using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Permissions;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Microsoft.Win32;

namespace LoneyCraftLauncher
{
    [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
    public class Library
    {
        private static INI ini = new INI(Guide(MAIN.LONEYCRAFTPATH + "LoneyCraftLauncher.ini"));

        /// <summary>
        /// Уведомляет пользователя сообщением об ошибке с последующим закрытием программы.
        /// </summary>
        public static void NotifyErrorAndCloseApplication(string title)
        {
            try
            {
                MessageBox.Show("В работе приложения обнаружена ошибка. После нажатия на кнопку ОК, информация об ошибке будет отправлена на наш сервер, а приложение LoneyCraft закрыто.\n\n" + title, "Ошибка в приложении LoneyCraft", MessageBoxButtons.OK, MessageBoxIcon.Error);

                WebRequest request = WebRequest.Create(MAIN.URLHTTP + "error.php");
                request.Method = "POST";
                request.Timeout = 120000;
                request.ContentType = "application/x-www-form-urlencoded";
                byte[] message = Encoding.UTF8.GetBytes("title=" + Encrypt(title) + "&version=" + Encrypt(GetPublishedVersion().ToString()));
                request.ContentLength = message.Length;
                Stream sendStream = request.GetRequestStream();
                sendStream.Write(message, 0, message.Length);
                sendStream.Close();
            }
            catch { }
            Environment.Exit(0);
        }

        public static void FormOpen(string form)
        {
            MAIN._GENERAL.Visible = false;
            MAIN._UPDATE.Visible = false;

            switch (form)
            {
                case "_GENERAL":
                    MAIN._GENERAL.Visible = true;
                    MAIN._GENERAL.FormOpen();
                    break;
                case "_UPDATE":
                    MAIN._UPDATE.Visible = true;
                    MAIN._UPDATE.FormOpen();
                    break;
                default:
                    MAIN._GENERAL.FormOpen();
                    break;
            }
        }

        /*
        public static string ExecuteCommand(string command)
        {
            Process cmd = new Process();
            cmd.StartInfo.FileName = "cmd";
            cmd.StartInfo.Arguments = "/c " + command;
            cmd.StartInfo.UseShellExecute = false;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.StandardOutputEncoding = Encoding.GetEncoding(866);
            cmd.Start();
            string st = cmd.StandardOutput.ReadToEnd();
            cmd.Close();
            return st;
        } */

        public static Version GetPublishedVersion()
        {
            XmlDocument xmlDoc = new XmlDocument();
            Assembly asmCurrent = System.Reflection.Assembly.GetExecutingAssembly();
            string executePath = new Uri(asmCurrent.GetName().CodeBase).LocalPath;

            xmlDoc.Load(executePath + ".manifest");
            string retval = string.Empty;
            if (xmlDoc.HasChildNodes)
            {
                retval = xmlDoc.ChildNodes[1].ChildNodes[0].Attributes.GetNamedItem("version").Value.ToString();
            }
            return new Version(retval);
        }

        public static string JavaPath()
        {
            // Поиск в реестре
            try
            {
                Lib.Registry registry = new Lib.Registry();
                RegistryKey jre;

                RegistryKey software = registry._openSubKey(Registry.LocalMachine, "SOFTWARE", false, Lib.Registry.RegWow64Options.KEY_WOW64_64KEY);
                RegistryKey javasoft = registry._openSubKey(software, "JavaSoft", false, Lib.Registry.RegWow64Options.KEY_WOW64_64KEY);
                jre = registry._openSubKey(javasoft, "Java Runtime Environment", false, Lib.Registry.RegWow64Options.KEY_WOW64_64KEY);

                // Поиск в реестре
                foreach (string version in Lib.Registry.GetSubKeyNames(Lib.Registry.HKEY_LOCAL_MACHINE, @"SOFTWARE\JavaSoft\Java Runtime Environment\", Lib.Registry.KEY_READ | Lib.Registry.KEY_WOW64_64KEY))
                {
                    RegistryKey inkey = registry._openSubKey(jre, version, false, Lib.Registry.RegWow64Options.KEY_WOW64_64KEY);
                    if (File.Exists((string)inkey.GetValue("JavaHome") + @"\bin\javaw.exe"))
                    {
                        return (string)inkey.GetValue("JavaHome") + @"\bin\javaw.exe";
                    }
                }
            }
            catch { }

                
            // Поиск в %PATH%
            String[] folders = Environment.GetEnvironmentVariable("PATH").Split(';');

            foreach (string folder in folders)
            {
                if (folder.Length > 1)
                {
                    if (folder[folder.Length - 1] == '\\')
                    {
                        if (File.Exists(folder + "javaw.exe"))
                        {
                            return folder + "javaw.exe";
                        }
                    }
                    else
                    {
                        if (File.Exists(folder + "\\javaw.exe"))
                        {
                            return folder + "\\javaw.exe";
                        }
                    }
                }
            }

            return null;
        }

        public static string Guide(string location)
        {
            string dir = location.Substring(0, location.LastIndexOf(@"\"));
            if (!Directory.Exists(dir))
            {
                DirectoryCreate(dir);
            }
            return location;
        }

        public static string LoadKey(string key)
        {
            return ini.IniReadValue("LoneyCraft", key);
        }

        public static void SaveKey(string key, string data)
        {
            ini.IniWriteValue("LoneyCraft", key, data);
        }

        public static string Encrypt(string source)
        {
            if (source.Length == 0)
            {
                return null;
            }
            if (source[0] == '.')
            {
                return source;
            }

            string decryp = "";
            char ch;
            for (int i = 0; i < source.Length; i++)
            {
                ch = source[i];
                ch++;
                decryp += Convert.ToString(Convert.ToInt32(ch) * (i + 16)) + ".";
            }
            return "." + decryp;
        }

        public static string Decrypt(string ToDecrypt)
        {
            if (ToDecrypt.Length == 0)
            {
                return null;
            }
            if (ToDecrypt[0] != '.')
            {
                return ToDecrypt;
            }

            string encrypt = "";
            string temp;
            char ch;
            ToDecrypt = ToDecrypt.Substring(1, ToDecrypt.Length - 1);
            for (int i = 0; i < 32; i++)
            {
                if (ToDecrypt.IndexOf(".") == -1)
                {
                    break;
                }
                temp = ToDecrypt.Substring(0, ToDecrypt.IndexOf(".") + 1);
                ToDecrypt = ToDecrypt.Substring(temp.Length, ToDecrypt.Length - temp.Length);
                ch = (char)(Convert.ToInt32(temp.Substring(0, temp.Length - 1)) / (i + 16));
                ch--;
                encrypt += Convert.ToString(ch);
            }
            return encrypt;
        }

        public static void FileDelete(string file)
        {
            if (File.Exists(file))
            {
                File.Delete(file);
            }
        }

        public static void FileCopy(string of, string to)
        {
            try
            {
                if (File.Exists(to))
                {
                    File.Delete(to);
                }
                File.Copy(of, to);
            }
            catch (IOException exception)
            {
                if (MessageBox.Show(exception.Message, "Ошибка в приложении LoneyCraft", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Retry)
                {
                    FileCopy(of, to);
                }
                else
                {
                    Environment.Exit(-1);
                }
            }
        }

        public static void DirectoryRemove(string path)
        {
            if (Directory.Exists(path))
            {
                try
                {
                    DirectoryInfo dir = new DirectoryInfo(path);
                    dir.Delete(true);
                }
                catch { }
            }
        }

        public static void DirectoryCreate(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public static void DirectoryClean(string path)
        {
            if (Directory.Exists(path))
            {
                DirectoryRemove(path);
                DirectoryCreate(path);
            }
        }

        public static string getHTML(string uri)
        {
            StringBuilder sb = new StringBuilder();
            byte[] buf = new byte[8192];
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream resStream = response.GetResponseStream();
            int count = 0;
            do
            {
                count = resStream.Read(buf, 0, buf.Length);
                if (count != 0)
                {
                    sb.Append(Encoding.UTF8.GetString(buf, 0, count));
                }
            }
            while (count > 0);
            return sb.ToString();
        }

        public static bool DownloadFromFTP(string _from, string _to, string _login = "anonymous", string _password = "", bool _keepalive = false, bool _usepassive = false, int _timeout = 60000)
        {
            try
            {
                Uri uri = new Uri(_from);
                if (uri.Scheme != Uri.UriSchemeFtp)
                {
                    return false;
                }
                FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(uri);
                request.Credentials = new NetworkCredential(_login, _password);
                request.KeepAlive = _keepalive;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.UseBinary = true;
                request.Proxy = null;
                request.UsePassive = _usepassive;
                request.Timeout = _timeout;
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                if (_to.IndexOf("/") == -1)
                {
                    DirectoryCreate(_to.Substring(0, _to.LastIndexOf("\\")));
                }
                else
                {
                    DirectoryCreate(_to.Substring(0, _to.LastIndexOf("/")));
                }

                using (FileStream writeStream = new FileStream(_to, FileMode.Create))
                {
                    int Length = 2048;
                    Byte[] buffer = new Byte[Length];
                    int bytesRead = responseStream.Read(buffer, 0, Length);
                    while (bytesRead > 0)
                    {
                        writeStream.Write(buffer, 0, bytesRead);
                        bytesRead = responseStream.Read(buffer, 0, Length);
                    }
                }
                return true;
            }
            catch (WebException)
            {
                return false;
            }
        }

        public static string GetMD5HashFromFile(string fileName)
        {
            using (FileStream file = new FileStream(fileName, FileMode.Open))
                using (MD5 md5 = new MD5CryptoServiceProvider())
                {
                    byte[] retVal = md5.ComputeHash(file);

                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < retVal.Length; i++)
                        sb.Append(retVal[i].ToString("x2"));

                    return sb.ToString();
                }
        }
    }
}
